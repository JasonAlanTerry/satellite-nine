"""
Characters

Characters are (by default) Objects setup to be puppeted by Accounts.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
from evennia import DefaultCharacter

from evennia.utils.utils import (lazy_property)

from characters.detail_handler import DetailHandler


class Character(DefaultCharacter):
    """
    The Character defaults to reimplementing some of base Object's hook methods with the
    following functionality:

    at_basetype_setup - always assigns the DefaultCmdSet to this object type
                    (important!)sets locks so character cannot be picked up
                    and its commands only be called by itself, not anyone else.
                    (to change things, use at_object_creation() instead).
    at_after_move(source_location) - Launches the "look" command after every move.
    at_post_unpuppet(account) -  when Account disconnects from the Character, we
                    store the current location in the pre_logout_location Attribute and
                    move it to a None-location so the "unpuppeted" character
                    object does not need to stay on grid. Echoes "Account has disconnected"
                    to the room.
    at_pre_puppet - Just before Account re-connects, retrieves the character's
                    pre_logout_location Attribute and move it back on the grid.
    at_post_puppet - Echoes "AccountName has entered the game" to the room.

    """

    """
    Handlers

    Planned handlers for the Player Character TypeClass

    obj.detail - Details - Description and Detail handler
    obj.info - Info - Encompass logic and data surrouding Class & Background Levels and information
    obj.imp - Improvement - Encompass logic and data surrouding experince points
    obj.attr - Attributes - Encompass logic and data surrouding attributes
    obj.health - Health - Encompass logic surrouding health and HP -> Wrapped Into Condition
    obj.cond - Condition - Encompass logic surrouding conditions
    obj.saves - Saves - Encompass logic surrouding character Saves
    obj.skills - Skills - Encompass logic surrouding skills
    obj.inv - Inventory - Encompass logic surrouding weapons, armor, ready items, and inventory space
    """
    @lazy_property
    def desc(self):
        return DetailHandler(self)
    @lazy_property
    def info(self):
        return InfoHandler(self)
    @lazy_property
    def foci(self):
        return FocusHandler(self)
    @lazy_property
    def imp(self):
        return ImprovementHandler(self)
    @lazy_property
    def attr(self):
        return AttributeHandler(self)
    @lazy_property
    def cond(self):
        return ConditionHandler(self)
    @lazy_property
    def save(self):
        return SaveHandler(self)
    @lazy_property
    def skill(self):
        return SkillsHandler(self)
    @lazy_property
    def inv(self):
        return InventoryHandler(self)

    # Init the charartes DB attributes.

    def init_character_data(self):
        pass

    def return_appearance(self, looker, **kwargs):
        if not looker:
            return ""
        return self.desc.gen_desc(self)
