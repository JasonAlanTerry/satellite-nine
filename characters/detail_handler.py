class DetailHandler():
    """
    Handler intended to encompass character description and detail logic and data.
    """
    def __init__(self, owner):
        self.owner = owner

    def gen_desc(self):
        """
        Dynamically generate a characters description based on state. Return
        
        """
        owner = self.owner
        desc = "You see a featureless entity."
        return desc