from tests.test_resources import EvenniaTest
# the function we want to test


class TestDetailHandler(EvenniaTest):
    "Testing DetailHandler functionality:"

    def test_gen_description_call(self):
        "Testing gen_desc method call."
        actual_return = self.char1.desc.gen_desc()
        # Assert return value is not None
        self.assertIsNotNone(actual_return)